# evil_dice_app

--------------------------------------------------------------------------------

# License Information

The copyright of the this repository is protected under the 'CC BY-NC' license as **Fernando Franke**, the only creator of the content.

##### This means that you are free to share and adapt the content for non-commercial purposes only, as long as you give appropriate credit to Fernando and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

For more information, lease refere to:

- [CC BY-NC License](https://creativecommons.org/licenses/by-nc/4.0/)
- [Fernando Franke - itch.io](https://steamless.itch.io/)


--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

A new Flutter project?

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


--------------------------------------------------------------------------------

