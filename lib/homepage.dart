import 'dart:math';

import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  AssetImage diceImage1 = AssetImage("images/dice1.png");
  AssetImage diceImage2 = AssetImage("images/dice2.png");
  AssetImage diceImage3 = AssetImage("images/dice3.png");
  AssetImage diceImage4 = AssetImage("images/dice4.png");
  AssetImage diceImage5 = AssetImage("images/dice5.png");
  AssetImage diceImage6 = AssetImage("images/dice6.png");

  AssetImage logo = AssetImage("images/logo.gif");
  AssetImage backgroundImage = AssetImage('images/bg.png');
  AssetImage rollButton = AssetImage('images/button_normal.png');

  late AssetImage dice1;
  late AssetImage dice2;
  late AssetImage dice3;
  late AssetImage dice4;
  late AssetImage dice5;
  late AssetImage dice6;

  late int diceImageSize = 4;

  late List diceAmountList = [
    dice1,
    dice2,
    dice3,
    dice4,
    dice5,
    dice6,
  ];
  
  @override
  void initState() {
    super.initState();

    // Set initial dice image state
    setState(() {
      dice1 = diceImage1;
      dice2 = diceImage2;
      dice3 = diceImage3;
      dice4 = diceImage4;
      dice5 = diceImage5;
      dice6 = diceImage6;
    });
  }

  int setRandomDiceValue() {
      return (Random().nextInt(6));
    }

  AssetImage setRandomDiceImage() {
    List<AssetImage> diceImagesList = [
      AssetImage('images/dice1.png'),
      AssetImage('images/dice2.png'),
      AssetImage('images/dice3.png'),
      AssetImage('images/dice4.png'),
      AssetImage('images/dice5.png'),
      AssetImage('images/dice6.png'),
    ];

    int randomIndexValue = setRandomDiceValue();

    int imageIndex;
    if (randomIndexValue >= 0 && randomIndexValue < 6) {
      imageIndex = randomIndexValue;
    } else {
      throw Exception('Invalid random value');
    }

    return diceImagesList[imageIndex];
  }

  void rollDice() {
    //TODO: set radiobuttons to activate dice and iterate through active dice
    AssetImage newDiceImage1 = setRandomDiceImage();
    AssetImage newDiceImage2 = setRandomDiceImage();
    AssetImage newDiceImage3 = setRandomDiceImage();
    AssetImage newDiceImage4 = setRandomDiceImage();
  
    setState(() {
      dice1 = newDiceImage1;
      dice2 = newDiceImage2;
      dice3 = newDiceImage3;
      dice4 = newDiceImage4;
    });
  }

  @override
  Widget build(BuildContext context) {
    final displaySize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: null,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: backgroundImage,
            fit: BoxFit.cover,
          )
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image(
                image: logo, 
                alignment: Alignment.topCenter
                ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,

                // TODO: call foreach method to generate an imageWidget based on amount of images
                children: [
                  Image(
                        image: dice1,
                        width: displaySize.width / diceImageSize,
                        height: displaySize.height / diceImageSize,
                      ),
                  Image(
                    image: dice2,
                    width: displaySize.width / diceImageSize,
                    height: displaySize.height / diceImageSize,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image(
                    image: dice3,
                    width: displaySize.width / diceImageSize,
                    height: displaySize.height / diceImageSize,
                  ),
                  Image(
                    image: dice4,
                    width: displaySize.width / diceImageSize,
                    height: displaySize.height / diceImageSize,
                  ),
                ],
              ),

              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceAround,
              //   crossAxisAlignment: CrossAxisAlignment.center,
              //   children: [
              //     Image(
              //       image: dice5,
              //       width: displaySize.width / diceImageSize,
              //       height: displaySize.height / diceImageSize,
              //     ),
              //     Image(
              //       image: dice6,
              //       width: displaySize.width / diceImageSize,
              //       height: displaySize.height / diceImageSize,
              //     ),
              //   ],
              // ),

              Material(
                  color: Colors.transparent,
                  child: 
                  InkWell(
                    onTap: rollDice,
                    customBorder: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50.0),
                    ),
                    child: Ink.image(
                      image: AssetImage("images/button_normal.png"),
                      fit: BoxFit.cover,
                      width: 200,
                      height: 100,
                      ),
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
}

class Dice {

}
