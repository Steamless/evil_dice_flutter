import 'package:flutter/material.dart';
import 'homepage.dart';

void main() => runApp(EvilDice());

class EvilDice extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Evil dice game',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: HomePage(),
    );
  }
}
